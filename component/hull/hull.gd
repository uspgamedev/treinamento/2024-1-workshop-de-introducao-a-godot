class_name Hull
extends Node

signal damaged

@export var defense := 1
@export var disabled_components: Array[Node]


func _on_damage(_source, power: int):
	if defense > 0:
		damaged.emit()
		defense = max(defense - power, 0)
		if defense == 0:
			for node in disabled_components:
				node.queue_free()
			await get_tree().create_timer(1.0).timeout
			owner.queue_free()
