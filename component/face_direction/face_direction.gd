class_name FaceDirection
extends Node

@export var align_with_movement: Movement


func _physics_process(_delta):
	if align_with_movement and not align_with_movement.velocity.is_zero_approx():
			owner.rotation = -align_with_movement.velocity.angle_to(Vector2.UP)
