class_name Movement
extends Node

@export var velocity: Vector2


func _physics_process(delta: float):
	if owner is CharacterBody2D:
		owner.move_and_collide(velocity * delta)
