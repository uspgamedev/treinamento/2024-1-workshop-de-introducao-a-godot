class_name ShipControlUserInput
extends ShipControl


func _get_push_force() -> float:
	return Input.get_axis(&'move_down', &'move_up')

func _get_steer_force() -> float:
	return Input.get_axis(&'move_left', &'move_right')

func _is_shooting() -> bool:
	return Input.is_action_pressed(&'shoot')
