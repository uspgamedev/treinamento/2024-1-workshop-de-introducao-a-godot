class_name ShipControl
extends Node

@export var movement: Movement
@export var shooters: Array[Spawn]

## Pixels per second
@export var push_speed_max := 200.0

## Pixels per second squared
@export var push_acceleration := 400.0

## Degrees per second
@export var steer_speed_max := 120.0

## Degrees per second squared
@export var steer_acceleration := 120.0

var _direction := Vector2.UP
var _push_speed := 0.0
var _steer_speed := 0.0


func _physics_process(delta: float):
	if movement:
		var push_force := _get_push_force()
		if push_force > 0:
			_push_speed = move_toward(
				_push_speed, push_force * push_speed_max, push_acceleration * delta
			)
		else:
			_push_speed = move_toward(
				_push_speed, 0, push_acceleration * -push_force * delta
			)
		if _push_speed > 0.0:
			var steer_force := _get_steer_force()
			_steer_speed = move_toward(
				_steer_speed, steer_force * steer_speed_max, steer_acceleration * delta
			)
			_direction = _direction.rotated(deg_to_rad(_steer_speed) * delta)
		movement.velocity = _direction * _push_speed
	if _is_shooting():
		for shooter in shooters:
			shooter.spawn()


func _get_push_force() -> float:
	return 0.0


func _get_steer_force() -> float:
	return 0.0


func _is_shooting() -> bool:
	return false
