class_name ShipControlFollowGroup
extends ShipControl

@export var target_group: StringName

var _target: Node2D


func _physics_process(delta):
	if owner is Node2D:
		var target: Node2D = null
		var min_dist := INF
		for node: Node2D in get_tree().get_nodes_in_group(target_group):
			if node:
				var dist: float = owner.position.distance_squared_to(node.position)
				if dist < min_dist:
					target = node
					min_dist = dist
		_target = target
	super(delta)


func _get_push_force() -> float:
	return 1.0 if _target else -1.0


func _get_steer_force() -> float:
	if owner is Node2D:
		if _target:
			var angle := _direction.angle_to(_target.position - owner.position)
			return clampf(angle, -1.0, 1.0)
	return 0.0
