class_name Damage
extends Node

@export var power := 1


func apply(target: Node2D):
	target.propagate_call("_on_damage", [owner, power])
