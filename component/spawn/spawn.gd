class_name Spawn
extends Marker2D

signal spawned(node: Node2D)

@export var spawn_scn: PackedScene
@export var cooldown: float = 0.0
@export var initial_movement: Vector2

var _timer: SceneTreeTimer = null


func spawn():
	if _timer:
		return
	if owner is Node2D:
		var node := spawn_scn.instantiate()
		node.position = owner.transform * position
		var movement: Movement = node.get_node_or_null('Movement')
		if movement:
			movement.velocity = owner.transform.basis_xform(initial_movement)
		_timer = get_tree().create_timer(cooldown)
		_timer.timeout.connect(func (): _timer = null)
		spawned.emit(node)
