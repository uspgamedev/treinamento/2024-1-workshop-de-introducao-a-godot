class_name HitBox
extends Area2D

signal hit(entity: Node2D)


func _ready():
	area_entered.connect(self._on_hit)


func _on_hit(other_hitbox: Area2D):
	if other_hitbox is HitBox:
		hit.emit(other_hitbox.owner)
