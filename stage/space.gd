class_name Space
extends Node2D


func _ready():
	_connect_spawners(self)


func _connect_spawners(node: Node):
	if node.has_signal(&'spawned'):
		node.spawned.connect(self._on_spawned)
	for child in node.get_children():
		_connect_spawners(child)


func _on_spawned(node: Node2D):
	_connect_spawners(node)
	add_child(node)
