# Workshop Introdução à Godot 2024.1

Os assets usados fazem parte da [coleção do Kenney](https://kenney.nl/assets).

## Estrutura do Workshop

Essa parte é mais um apoio para o ministrante e não como material de referência,
mas vou deixar aqui junto porque pode ajudar mesmo assim.

### 1. Apresentando a Godot

* O que é e para quê serve?
* Como instalar?
  + Site oficial
  + Steam
* Como uso o editor?
  + Partes da interface
    - Editor principal 
    - Barra de menu
    - Docas
    - Painel inferior
  + Configurações do projeto
* Como crio jogos com a Godot?
  + Cenas
  + Executando cenas
  + Cena principal
* Como lanço jogos feitos na Godot?
  + Plataformas
  + Exportação

### 2. Nós e cenas

* Como construo cenas usando nós?
  + Criação nós
  + Composição de transformações
  + NodePaths
  + Cenas instanciadas (prefabs)
  + Cenas herdadas (especialização)
* Como uso nós para criar coisas no jogo?
  + Propriedades
  + Recursos
  + Métodos
  + Sinais
  + Scripts
* Que tipos de nós existem?
  + Node2D: Sprite, Tilemap, Polygon2D
  + Node3D: Mesh, Gridmap, Sprite3D
  + Control: Labels, Buttons, Containers
  + AudioStreamPlayer
  + AnimationPlayer
  + Timer

### 3. Demonstração

1. Nave que se move em uma direção
2. Nave que aponta para a direção em que se move
3. Nave cuja direção é controlada pelo jogador
4. Nave cuja direção é determinada para seguir o jogador
5. Detecção de colisão via hitbox
6. Destruição de objetos
7. Naves que são criadas por tempo
8. Nave que atira laser

### 4. Extra

* Depuração
  + Colisões
  + Árvore remota
  + Depurador de código
* Godot é feita em Godot
  + Nós do tipo @tool
  + Plugins

### Tarefas de casa

* Todos: criar uma nave "Enemy Cruiser" *sem usar código*
  + bastante vida
  + lança mísseis teleguiados e fighters em períodos de tempo diferentes
  + aparece no jogo com frequência menor que os fighters
  + se move em linha reta e devagar
  + DESAFIO: se auto-deleta eventualmente (tem vários jeitos de fazer)
* Artistas: usar CPUParticles2D ou GPUParticles2D para fazer um "rastro" no
  movimento das naves
* Sound designers: colocar sons na "Enemy Cruiser" e uma BGM
* Programadores: ler e entender todo o código
